/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as express from 'express';
import { WithAuth } from './Auth';
import { Label, PostImage } from './model/Image';
import { LabelService } from '../business/LabelService';
import { ScoreService } from '../business/ScoreService';
import * as log4js from 'log4js';

const log = log4js.getLogger('file');

const atob = (value: string) => {
  const buf = new Buffer(value, 'base64');
  let bytes = [];
  for (let i = buf.length - 1; i >= 0; i--) {
    bytes[i] = String.fromCharCode(buf[i]);
  }
  return bytes.join('').trim();
};

export class Image {
  private labelService = LabelService.instance;
  private scoreService = ScoreService.instance;

  public postV1 = (req: express.Request, res: express.Response) => {
    const data: PostImage = req.body;
    const label: Label = {
      ...data.label,
      boxes: data.label.boxes.map(b => ({
        ...b,
        start: {
          x: b.start.x / 640,
          y: b.start.y / 480
        },
        size: {
          x: b.size.x / 640,
          y: b.size.y / 480
        }
      }))
    };
    this.update(data.authToken, label, res);
  }

  public postV2 = (req: express.Request, res: express.Response) => {
    const data: PostImage = req.body;
    this.update(data.authToken, data.label, res);
  }

  private update(authToken: string | undefined, label: Label, res: express.Response) {
    if (authToken) {
      WithAuth(authToken, d => {
        this.scoreService.addScore(d.login, label);
      });
    }
    if (this.labelService.set(label)) {
      log.info(`Update image '${label.url}'`);
      res.sendStatus(200);
      return;
    }
    log.error(`Update image '${label.url}'`);
    res.sendStatus(400);
  }

  public getV1 = (req: express.Request, res: express.Response) => {
    this.labelService.getImage()
      .then(l => ({
        ...l,
        boxes: l.boxes.map(b => ({
          ...b,
          start: {
            x: b.start.x * 640,
            y: b.start.y * 480
          },
          size: {
            x: b.size.x * 640,
            y: b.size.y * 480
          }
        }))
      }))
      .then(l => {
        res.json(l);
      })
      .catch(() => {
        res.sendStatus(404);
      });
  }

  public getV2 = (req: express.Request, res: express.Response) => {
    var imageUrl: string|undefined = undefined;
    if (req.query.image) {
      try {
        imageUrl = atob(req.query.image);
      } catch (e) {
        log.info(`Not a base64: '${req.query.image}'`);
      }
    }
    this.labelService.getImage(imageUrl)
      .then(l => {
        res.json(l);
      })
      .catch(() => {
        res.sendStatus(404);
      });
  }

  public getStats = (req: express.Request, res: express.Response) => {
    this.labelService.getStats()
      .then(stats => res.json(stats));
  }
}
