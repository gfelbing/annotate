/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import axios from 'axios';
import { ClientIDGet, LoginPostResponse } from './model/Login';
import { GetAuth } from './model/Image';
import * as express from 'express';
import { ConfigService } from '../business/ConfigService';

export interface GithubV3User {
  login: string;
}

export class Auth {
  private config: ConfigService;

  public getClientID = (req: express.Request, res: express.Response) => {
    const response: ClientIDGet = {
      id: this.config.getAuth().clientID
    };
    res.json(response);
  }

  public postAuth = (req: express.Request, res: express.Response) => {
    const data: GetAuth = req.body;
    if (data.code) {
      const clientId = this.config.getAuth().clientID;
      const clientSecret = this.config.getAuth().secret;
      console.log('auth using ' + clientId);
      axios.post('https://github.com/login/oauth/access_token', {
        client_id: clientId,
        client_secret: clientSecret,
        code: data.code,
      })
        .then(r => {
          const token: string[] | undefined = r.data
            .split('&')
            .map((p: string) => p.split('='))
            .find((p: string[0]) => p[0] === 'access_token');
          if (token) {
            axios
              .get('https://api.github.com/user', {
                params: {
                  access_token: token[1]
                }
              })
              .then(response => response.data as GithubV3User)
              .then(d => {
                const response: LoginPostResponse = {
                  accessToken: token[1],
                  login: d.login
                };
                res.json(response);
              });
          } else {
            res.sendStatus(400);
          }
        })
        .catch(e => res.sendStatus(e.status));
    }
  }

  public constructor() {
    this.config = ConfigService.instance;
  }
}

export const WithAuth = (authToken: string, cb: (u: GithubV3User) => void) => axios
  .get('https://api.github.com/user', {
    params: {
      access_token: authToken
    }
  })
  .then(r => r.data as GithubV3User)
  .then(cb);
