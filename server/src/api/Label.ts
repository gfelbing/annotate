/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as express from 'express';
import { ConfigService } from '../business/ConfigService';
import { LabelsV2 } from './model/LabelsV2';

export class Label {
  protected labelService = ConfigService.instance;

  public getV1 = (req: express.Request, res: express.Response) => {
    res.json(this.labelService.getLabels().boxes);
  }

  public getV2 = (req: express.Request, res: express.Response) => {
    const labels = this.labelService.getLabels();
    const data: LabelsV2 = {
      images: labels.images,
      boxes: labels.boxes
    };
    res.json(data);
  }

  public getV3 = (req: express.Request, res: express.Response) => {
    res.json(this.labelService.getLabels());
  }
}
