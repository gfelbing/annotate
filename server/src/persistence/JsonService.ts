/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import Lock from '../util/Locks';
import * as fs from 'fs';

export class JsonService<I, T> {
  private file: string;
  private content: T;
  private writeLock: Lock;

  constructor(file: string, defaultContent: I, migrations?: Migration<Migratable>[]) {
    this.file = file;
    this.migrate(defaultContent, migrations);
    this.content = this.load();
    this.writeLock = new Lock(`${this.file}.lock`);
  }

  update(f: (c: T) => T) {
    this.writeLock.lock(() => {
      const newConfig = f(this.content);
      fs.copyFileSync(this.file, `${this.file}.bak`);
      fs.writeFileSync(this.file, JSON.stringify(newConfig));
      this.content = newConfig;
    });
  }

  get(): T {
    return this.content;
  }

  private migrate(defaultContent: I, migrations?: Migration<Migratable>[]) {
    if (!fs.existsSync(this.file)) {
      console.warn(`'${this.file}' could not be found.`);
      console.warn(`Continue with default config: '${JSON.stringify(defaultContent)}'`);
      fs.writeFileSync(this.file, JSON.stringify(defaultContent));
    }
    if (!migrations || migrations.length === 0) {
      return;
    }
    migrations.forEach(m => {
      const object = JSON.parse(fs.readFileSync(this.file, 'utf-8'));
      const migratable: Migratable = {
        ...object,
        migrations: object.migrations ? object.migrations : []
      };
      if (migratable.migrations.indexOf(m.key) < 0) {
        console.log(`Applying migration '${m.key}' to '${this.file}'`);
        const migrated = m.apply(migratable);
        migrated.migrations.push(m.key);
        fs.copyFileSync(this.file, `${this.file}.${m.key}`);
        fs.writeFileSync(this.file, JSON.stringify(migrated));
      }
    });
  }

  private load(): T {
    const rawContent = fs.readFileSync(this.file, 'utf-8');
    return JSON.parse(rawContent);
  }

}

export interface Migratable {
  migrations: string[];
}

export interface Migration<T extends Migratable> {
  key: string;

  apply(f: {}): T;
}
