/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as path from 'path';
import Lock from '../util/Locks';
import * as Loki from 'lokijs';
import * as fs from 'fs';

export interface AnnotationEntry<T> {
  jsonFile: string;
  imageFile: string;
  data: T;
}

interface JsonVersion {
  jsonFile: string;
  version: number;
}

export class LokiJsonAdapter<T> {
  versionIndexCollection = 'versions';
  mode = 'reference';

  outerCollection: string;
  defaultEntry: (url: string) => T;
  version: (entry: T) => number;
  versionIndex: Loki;

  constructor(
    outerCollection: string,
    defaultEntry: (url: string) => T,
    entryVersion: (entry: T) => number
  ) {
    this.outerCollection = outerCollection;
    this.defaultEntry = defaultEntry;
    this.version = entryVersion;
    this.versionIndex = new Loki('');
  }

  getCollection(): Collection<JsonVersion> {
    if (this.versionIndex.getCollection(this.versionIndexCollection) == null) {
      this.versionIndex.addCollection(this.versionIndexCollection);
    }
    return this.versionIndex.getCollection(this.versionIndexCollection);
  }

  exportDatabase(dbname: string, dbref: Loki, callback: (a: null | Error) => void) {
    const versions = this.getCollection();
    dbref.getCollection<AnnotationEntry<T>>(this.outerCollection)
      .where(entry => versions.findOne(({
        jsonFile: entry.jsonFile,
        version: {$lt: this.version(entry.data)}
      })) != null)
      .forEach(entry => {
        versions.chain()
          .find({jsonFile: entry.jsonFile})
          .update(v => {
            v.version = this.version(entry.data);
          });
        const jsonPath = path.join(dbname, entry.jsonFile);
        new Lock(`${jsonPath}.lck`).lock(() => {
          console.log(`Saving '${jsonPath}'`);
          fs.writeFileSync(jsonPath, JSON.stringify(entry.data));
        });
      });
    callback(null);
    // TODO: handle errors: callback(new Error("some error occurred."));
  }

  loadDatabase(dbname: string, callback: (value: Loki | Error) => void): void {
    console.log(`Loading Database '${dbname}'`);
    const database = new Loki(dbname);
    const collection = database.addCollection(this.outerCollection);
    this.walk(dbname, file => {
      const data = this.loadFile(dbname, file);
      if (data) {
        console.log(`Indexing '${data.imageFile}'`);
        collection.insert(data);
      }
    });
    callback(database);
    // TODO: handle errors: callback(new Error("some error occurred."));
  }

  private loadFile(dataDir: string, image: string): AnnotationEntry<T> | null {
    if (!image.toLowerCase().endsWith('png') || !fs.existsSync(image)) {
      return null;
    }
    const imageFile = path.relative(dataDir, image);
    const jsonFile = `${imageFile}.json`;

    const data = fs.existsSync(path.join(dataDir, jsonFile))
      ? JSON.parse(fs.readFileSync(path.join(dataDir, jsonFile), 'UTF-8'))
      : this.defaultEntry(imageFile);
    this.getCollection()
      .insert({
                jsonFile: jsonFile,
                version: this.version(data),
              });
    return {
      jsonFile: jsonFile,
      imageFile: imageFile,
      data: data,
    };
  }

  private walk(filePath: string, cb: ((n: string) => void)): void {
    for (let file of fs.readdirSync(filePath)) {
      let fullPath = path.join(filePath, file);
      const stats = fs.lstatSync(fullPath);
      if (stats.isDirectory()) {
        this.walk(fullPath, cb);
      }
      if (stats.isFile()) {
        cb(fullPath);
      }
    }
  }
}
