/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Label } from '../api/model/Image';
import * as Loki from 'lokijs';
import { Collection } from 'lokijs';
import { getDataDir } from '../business/Util';
import { Stats } from '../api/model/Stats';
import { AnnotationEntry, LokiJsonAdapter } from './LokiJsonAdapter';

export class AnnotationDao {
  private database: Loki;

  constructor(defaultEntry: (url: string) => Label) {
    this.database = new Loki(getDataDir(), {
      adapter: new LokiJsonAdapter(
        'annotations',
        defaultEntry,
        e => e.labelCount ? e.labelCount : 0
      ),
      autoload: true,
      autosave: true,
      autosaveInterval: 10 * 60 * 1000, // Every 10min
      verbose: true,
    });

    process.on('SIGINT', this.closeDatabase.bind(this));
    process.on('exit', this.closeDatabase.bind(this));
  }

  closeDatabase() {
    console.log('Closing database...');
    this.database.close(error => {
      if (error) {
        console.error('Error closing database', error);
      }
    });
    console.log('Done.');
    process.exit();
  }

  getStats(): Promise<Stats> {
    const allEntries = this.getCollection().data.length;
    const labeled = this.getCollection()
      .chain()
      .where(entry => entry.data.labelCount ? entry.data.labelCount > 0 : false)
      .count();
    return new Promise((res, rej) => {
      res({
            labeled: labeled,
            remaining: allEntries - labeled,
          });
    });
  }

  set(key: string, data: Label) {
    return new Promise((resolve, reject) => {
      const updated = this.getCollection().chain()
        .find({imageFile: {$eq: key}})
        .update(entry => {
          const labelCount = entry.data.labelCount ? entry.data.labelCount + 1 : 1;
          entry.data = {
            ...data,
            labelCount: labelCount
          };
        })
        .count();
      if (updated === 0) {
        reject('Unknown key');
      } else {
        resolve();
      }
    });
  }

  get(key: string): Promise<[string, Label]> {
    return new Promise((resolve, reject) => {
      const result = this.getCollection().findOne({imageFile: {$eq: key}});
      if (result != null) {
        resolve([key, result.data]);
      } else {
        reject('Unknown key.');
      }
    });
  }

  getCollection(): Collection<AnnotationEntry<Label>> {
    return this.database.getCollection<AnnotationEntry<Label>>('annotations');
  }

  getRandom(): Promise<[string, Label]> {
    // TODO: optimize this somehow
    return new Promise((resolve, reject) => {
      const result = this.getCollection()
        .chain()
        .sort((a, b) => {
          if (!a.data.labelCount) {
            return -1;
          }
          if (!b.data.labelCount) {
            return 1;
          }
          return a.data.labelCount > b.data.labelCount ? 1 : -1;
        })
        .limit(1)
        .data();
      if (result.length > 0) {
        resolve([result[0].imageFile, result[0].data]);
      } else {
        reject('Nothing there');
      }
    });
  }

}
