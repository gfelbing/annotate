/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Label } from '../api/model/Image';
import { Migratable } from './JsonService';
import { LabelsV2 } from '../api/model/LabelsV2';
import { LabelsV3 } from '../api/model/LabelsV3';
import { Scores } from '../api/model/Scores';

export interface ConfigV1 {
  labels: string[];
}

export interface ConfigV2 extends Migratable {
  labels: LabelsV2;
}

export interface ConfigV3 extends Migratable {
  labels: LabelsV3;
}

export interface ConfigV4 extends ConfigV3 {
  inflation: number;
  auth: AuthConfig;
}

export interface ConfigV5 extends ConfigV3 {
  auth: AuthConfig;
}

export interface AuthConfig {
  clientID: string;
  secret: string;
}

export interface LabelData {
  data: {
    [key: string]: Label;
  };
}

export interface ScoresV2 extends Migratable {
  scores: Scores;
  sum: number;
}
