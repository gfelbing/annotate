/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Label } from '../api/model/Image';
import { getDataDir } from './Util';
import { Stats } from '../api/model/Stats';
import { AnnotationDao } from '../persistence/AnnotationDao';

export class LabelService {
  public static instance = new LabelService();
  private dataDir: string;
  private label: AnnotationDao;

  getImage(url?: string): Promise<Label> {
    if (url) {
      return this.label.get(url).then(fixUrl);
    } else {
      return this.label.getRandom().then(fixUrl);
    }
  }

  set(label: Label): Promise<{}> {
    return this.label.set(label.url, label);
  }

  getDataDir(): string {
    return this.dataDir;
  }

  getStats(): Promise<Stats> {
    return this.label.getStats();
  }

  private constructor() {
    this.dataDir = getDataDir();
    console.log(`Using ${this.dataDir}`);

    this.label = new AnnotationDao((url) => ({
      url: url,
      boxes: [],
      labelCount: 0,
    }));
  }
}

function fixUrl(tuple: [string, Label]): Label {
  return {
    ...tuple[1],
    url: tuple[0]
  };
}
