/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as path from 'path';
import { JsonService, Migratable, Migration } from '../persistence/JsonService';
import { getDataDir } from './Util';
import { AuthConfig, ConfigV1, ConfigV2, ConfigV3, ConfigV4, ConfigV5 } from '../persistence/Model';
import { LabelsV3 } from '../api/model/LabelsV3';
import { Option } from '../util/Types';

const initialConfig: ConfigV1 = {
  labels: ['ball', 'robot']
};

const configMigrations: Migration<Migratable>[] = [
  {
    key: 'v2',
    apply: (v1: ConfigV1): ConfigV2 => ({
      migrations: [],
      labels: {
        images: ['game', 'other'],
        boxes: v1.labels
      }
    })
  },
  {
    key: 'v3',
    apply: (v2: ConfigV2): ConfigV3 => ({
      migrations: v2.migrations,
      labels: {
        images: v2.labels.images,
        boxes: v2.labels.boxes,
        lines: ['line', 'goalpost']
      }
    })
  },
  {
    key: 'v4',
    apply: (v3: ConfigV3): ConfigV4 => ({
      ...v3,
      inflation: 1000,
      auth: {
        clientID: Option.of(process.env.OAUTH_CLIENT_ID).get(() => 'c1573e299e83048c274f'),
        secret: Option.of(process.env.OAUTH_SECRET).get(() => 'unset')
      }
    })
  },
  {
    key: 'v5',
    apply: (v4: ConfigV4): ConfigV5 => ({
      migrations: v4.migrations,
      labels: v4.labels,
      auth: v4.auth
    })
  }
];

export class ConfigService {
  public static instance = new ConfigService();
  private config: JsonService<ConfigV1, ConfigV4>;

  constructor() {
    this.config = new JsonService(
      path.join(getDataDir(), 'annotate.json'),
      initialConfig,
      configMigrations
    );
  }

  getLabels(): LabelsV3 {
    return this.config.get().labels;
  }

  getAuth(): AuthConfig {
    return this.config.get().auth;
  }

}
