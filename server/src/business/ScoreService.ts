/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Scores } from '../api/model/Scores';
import { getDataDir } from './Util';
import * as path from 'path';
import { JsonService, Migratable, Migration } from '../persistence/JsonService';
import { ScoresV2 } from '../persistence/Model';
import { Stream } from '../util/Types';
import { Label } from '../api/model/Image';

const initialScores: Scores = {};

const migrations: Migration<Migratable>[] = [
  {
    key: 'v2',
    apply: (v1: Scores): ScoresV2 => {
      delete v1.migrations;
      return {
        migrations: [],
        scores: v1,
        sum: Stream.ofMap(v1)
          .map(entry => entry.value)
          .fold<number>((a, b) => a + b, 0)
      };
    }
  }
];

export class ScoreService {
  public static instance = new ScoreService();
  private scores: JsonService<Scores, ScoresV2>;

  getScores(): Scores {
    return this.scores.get().scores;
  }

  addScore(user: string, label: Label): void {
    const linePoints = label.lines ? label.lines.length : 0;
    const boxPoints = label.boxes.length;
    const points = linePoints * 1.5 + boxPoints * 1.5 + 1;

    this.scores.update((c: ScoresV2) => {
      if (c.scores[user]) {
        c.scores[user] += points;
      } else {
        c.scores[user] = points;
      }
      c.sum += points;
      return c;
    });
  }

  private constructor() {
    this.scores = new JsonService<Scores, ScoresV2>(
      path.join(getDataDir(), 'scores.json'),
      initialScores,
      migrations);
  }
}
