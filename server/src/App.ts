/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as express from 'express';
import * as bodyParser from 'body-parser';
import { Auth } from './api/Auth';
import { Image } from './api/Image';
import { Label } from './api/Label';
import { Scores } from './api/Scores';
import { Statics } from './api/Statics';
import * as log4js from 'log4js';
import * as path from 'path';
import { getDataDir } from './business/Util';

log4js.configure({
                   appenders: {file: {type: 'file', filename: path.join(getDataDir(), 'annotate.log')}},
                   categories: {default: {appenders: ['file'], level: 'info'}}
                 });

class App {
  public express: express.Application;
  private image = new Image();
  private label = new Label();
  private score = new Scores();
  private statics = new Statics();
  private auth = new Auth();

  constructor() {
    this.express = express();
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({extended: true}));
    this.express.use(express.json());
    this.express.use(express.urlencoded());
    this.mountRoutes();
  }

  private mountRoutes(): void {
    this.express.post('/v1/auth', this.auth.postAuth);
    this.express.get('/v1/auth/clientId', this.auth.getClientID);

    this.express.post('/v1/image', this.image.postV1);
    this.express.get('/v1/image', this.image.getV1);
    this.express.get('/v1/stats', this.image.getStats);
    this.express.get('/v1/labels', this.label.getV1);
    this.express.get('/v1/scores', this.score.get);

    this.express.get('/v2/labels', this.label.getV2);
    this.express.get('/v2/image', this.image.getV2);
    this.express.post('/v2/image', this.image.postV2);

    this.express.get('/v3/labels', this.label.getV3);

    this.express.use(this.statics.statics);
    this.express.use(this.statics.images);
  }
}

export default new App().express;
