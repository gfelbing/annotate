/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ReducerFactory } from './util/Reducers';
import { connect, Dispatch } from 'react-redux';
import { RootState } from './index';
import { Actions, OwnProps, Props, State, KeyboardLayout } from './AppModel';
import { Component } from './AppView';
import { Option } from './util/Types';

const actions = new ReducerFactory<State, OwnProps>();

const UpdateKeyboard = actions.createAction<string>((state, action) => {
  localStorage.setItem('keyboard', action.value);
  return {
    ...state,
    keyboard: KeyboardLayout[action.value]
  };
});

const initialState: State = {
  keyboard: Option
    .of(localStorage.getItem('keyboard'))
    .map(s => KeyboardLayout[s])
    .get( () => KeyboardLayout.DEFAULT )
};

export const AppReducer = actions.build(initialState);

export const App = connect(
  (rootState: RootState, ownProps: OwnProps): Props => {
    return {
      ...ownProps,
      authToken: rootState.login.login.map(l => l.accessToken),
      state: {
        ...rootState.app
      }
    };
  },
  (dispatch: Dispatch<{}>, props: OwnProps): Actions => {
    return {
      onUpdateKeyboard: UpdateKeyboard.create(dispatch, props)
    };
  }
)(Component);
