/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Button } from 'react-bootstrap';
import * as React from 'react';
import { Actions, Props } from './LoginModel';
import { Option } from '../util/Types';
import { Lifecycle } from '../Lifecycle';

const getParameter = (param: string): Option<string> => {
  return Option.of(
    window.location.search
      .substr(1)
      .split('&')
      .map(pair => pair.split('='))
      .find(pair => pair[0] === param))
    .map(pair => pair[1]);
};

export const Component = (props: Props & Actions) => {
  return (
    <form>
      <Lifecycle
        componentDidMount={() => {
          getParameter('code')
            .filter(c => props.state.login.isEmpty())
            .filter(c => c !== props.state.code)
            .map(code => props.onCode(code));
          props.onLoad();
        }}
      />
      {props.state.login
        .map(login => (
          <Button
            onClick={() => props.onSignOut()}
          >
            ({login.login}) Logout
          </Button>
        ))
        .or(() => props.state.clientID
          .map(authorizeUrl)
          .map(url => (
            <Button
              onClick={() => {
                window.location.href = url;
              }}
            >
              Login with Github
            </Button>
          ))
        )
        .get(() => <div/>)
      }
    </form>
  );
};

const authorizeUrl = (clientId: string): string => {
  return `https://github.com/login/oauth/authorize?client_id=${clientId}`;
};
