/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import { Col, Jumbotron, Row, Table } from 'react-bootstrap';
import { KeyboardLayout } from '../AppModel';

export interface Props {
  keyboard: KeyboardLayout;
}

export const Readme = (props: Props) => (
  <Row>
    <Col xs={12} lg={6}>
      <Jumbotron>
        <h3>
          Keybindings
        </h3>
        <Table>
          <thead>
          <tr>
            <th>Key</th>
            <th>Effect</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>
              {props.keyboard === KeyboardLayout.DEFAULT
                ? '[Shift+]Arrows'
                : (<div>[Shift+]Arrows<br/>[Shift+] H/J/K/L</div>)
              }
            </td>
            <td>Move second control point. (Use Shift for fast mode)</td>
          </tr>
          <tr>
            <td>
              {props.keyboard === KeyboardLayout.DEFAULT
                ? 'Ctrl+[Shift+]Arrows'
                : (<div>Ctrl+[Shift+]Arrows<br/>Ctrl+[Shift+] H/J/K/L</div>)
              }
            </td>
            <td>Move first control point. (Use Shift for fast mode)</td>
          </tr>
          <tr>
            <td>Numbers</td>
            <td>Select the label</td>
          </tr>
          <tr>
            <td>
              {props.keyboard === KeyboardLayout.DEFAULT
                ? 'Letters'
                : 'F/D/S/A/V/C/X/Y'
              }
            </td>
            <td>Select the label,
              {props.keyboard === KeyboardLayout.DEFAULT
                ? 'according to first letter. (i.e., l → line)'
                : 'according to their order. (i.e., F → first label, D → second label)'
              }
            </td>
          </tr>
          <tr>
            <td>
              {props.keyboard === KeyboardLayout.DEFAULT
                ? 'Ctrl+Letters'
                : 'V/C/X/Y'
              }
            </td>
            <td>Select image label,
              {props.keyboard === KeyboardLayout.DEFAULT
                ? 'according to first letter. (i.e., o → other)'
                : 'according to their order. (i.e., F → first label, D → second label)'
              }
            </td>
          </tr>
          <tr>
            <td>
              {props.keyboard === KeyboardLayout.DEFAULT
                ? 'Enter'
                : 'Enter / Space'
              }
              </td>
            <td>Add current selection to labels</td>
          </tr>
          <tr>
            <td>
              {props.keyboard === KeyboardLayout.DEFAULT
                ? 'Shift+Enter'
                : <div>Shift+Enter<br/>Shift+Space</div>
              }
            </td>
            <td>Send image to server</td>
          </tr>
          <tr>
            <td>Escape</td>
            <td>Reset the current selection.</td>
          </tr>
          </tbody>
        </Table>
      </Jumbotron>
    </Col>
    <Col xs={12} lg={6}>
      <Jumbotron>
        <h3>How to label</h3>
        <ul>
          <li>Select the bounding boxes around objects that you can clearly identify.</li>
          <li>Surround the objects as close as possible, but completely.</li>
          <li>The robots own belly isn't a robot.</li>
          <li>The wooden goal support structure on the ground should be labeled as 'line'.</li>
          <li>An Image label annotates the whole situation on the image.</li>
          <ul>
            <li>Images, where the robot stands on the field in situations which could happen during games, should be
              labeled as 'game'
            </li>
            <li>Images, where you are not sure if it correct what you are doing, should be marked as 'unclear'.</li>
            <li>Every other situation should be labeled as 'other'</li>
            <li>Complete black or white images should be labeled as 'other'</li>
          </ul>
        </ul>
        <h4>Examples</h4>
        Here is a very sophisticated example:
        <img src="assets/example-sophisticated.png" width="80%"/>
        <img src="assets/example-sophisticated-labels.png" width="20%"/>
      </Jumbotron>
    </Col>
  </Row>
);
