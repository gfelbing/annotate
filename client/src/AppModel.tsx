/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Option } from './util/Types';

export enum KeyboardLayout {
  DEFAULT,
  FANCY
}

export interface OwnProps {
  authToken: Option<string>;
}

export interface State {
  keyboard: KeyboardLayout;
}

export interface Actions {
  onUpdateKeyboard: (e: string) => void;
}

export interface Props extends OwnProps {
  state: State;
}
