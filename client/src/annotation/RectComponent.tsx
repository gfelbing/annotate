/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { GenericPoint } from '../api/Point';
import * as React from 'react';

export const Rect = (props: {
  start: GenericPoint<string>;
  size: GenericPoint<string>;
  content?: string;
  color?: string;
  background?: string
}) => {
  return (
    <g>
      <rect
        x={props.start.x}
        y={props.start.y}
        width={props.size.x}
        height={props.size.y}
        style={{
          stroke: props.color ? props.color : 'black',
          strokeWidth: '2',
          fill: 'none'
        }}
      />
      {props.content
        ? <svg
          x={props.start.x}
          y={props.start.y}
          width={props.size.x}
          height={props.size.y}
        >
          <text
            fontWeight="bold"
            fill={props.color ? props.color : 'black'}
            x={0}
            y="1em"
          >
            {props.content}
          </text>
        </svg>
        : null
      }
    </g>
  );
};
