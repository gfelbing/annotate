/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import { Point } from '../api/Point';
import { HULKS_PINK } from './AnnotationModel';

export const Preview = (props: {
  imageUrl: string,
  stateStart: Point,
  stateSize: Point,
  top?: string,
  bottom?: string,
  left?: string,
  right?: string,
  content?: JSX.Element,
  width?: string
}) => {
  return (
    <div
      ref={e => handleRef(e, props.stateSize)}
      style={{
        overflow: 'hidden',
        borderColor: {HULKS_PINK},
        borderStyle: 'solid',
        width: props.width ? props.width : '20%',
        position: 'absolute',
        top: props.top,
        bottom: props.bottom,
        left: props.left,
        right: props.right
      }}
    >
      <img
        src={props.imageUrl}
        style={{
          transform: `translate(-${props.stateStart.x * 100}%,-${props.stateStart.y * 100}%)`,
          width: `${1 / props.stateSize.x * 100}%`,
          height: `${1 / props.stateSize.y * 100}%`
        }}
      />
      {props.content}
    </div>
  );
};

const handleRef = (e: HTMLDivElement | null, stateSize: Point) => {
  if (e) {
    const image = document.getElementById('labelImage');
    if (image) {
      const selectSize = {
        x: image.getBoundingClientRect().width * stateSize.x,
        y: image.getBoundingClientRect().height * stateSize.y
      };
      const zoomWidth = e.getBoundingClientRect().width;
      const selectRatio = selectSize.y / selectSize.x;
      e.style.height = `${zoomWidth * selectRatio}px`;
    }
  }
};
