/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Point } from '../api/Point';
import * as React from 'react';

export const Line = (props: { p1: Point, p2: Point, color?: string, label?: string }) => {
  return (
    <g>
      <line
        x1={`${props.p1.x * 100}%`}
        y1={`${props.p1.y * 100}%`}
        x2={`${props.p2.x * 100}%`}
        y2={`${props.p2.y * 100}%`}
        style={{
          stroke: props.color ? props.color : 'black',
          strokeWidth: '2'
        }}
      />
      {props.label
        ? <text
          x={`${props.p1.x * 100}%`}
          y={`${props.p1.y * 100}%`}
          fontWeight="bold"
          fill={props.color ? props.color : 'black'}
        >
          {props.label}
        </text>
        : null
      }
    </g>
  );
};
