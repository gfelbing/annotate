/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import { Rect } from './RectComponent';
import { Point } from '../api/Point';
import { ToPercent } from './AnnotationView';
import { HULKS_PINK } from './AnnotationModel';

export const RectSelector = (props: { p1: Point, p2: Point, stateStart: Point, stateSize: Point, label: string }) => {
  const p1p = ToPercent(props.stateStart);
  let sizep = ToPercent(props.stateSize);
  return (
    <g>
      <Rect
        start={p1p}
        size={sizep}
        content={props.label}
        color={HULKS_PINK}
      />
      <svg
        x={p1p.x}
        y={p1p.y}
        width={sizep.x}
        height={sizep.y}
      >
        <Rect start={{x: '0', y: '0'}} size={{x: '2%', y: '2%'}} color={HULKS_PINK}/>
        <Rect start={{x: '98%', y: '98%'}} size={{x: '2%', y: '2%'}} color={HULKS_PINK}/>
      </svg>
    </g>
  );
};
