/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import { Button, ButtonGroup, ControlLabel, DropdownButton, FormGroup, InputGroup, MenuItem } from 'react-bootstrap';
import { Option } from '../util/Types';

interface Props {
  title?: string;
  idAddon?: string;
  saveButton?: string;
  label: string;
  labels: string[];
  colors: string[];
}

interface Actions {
  onChangeLabel: (label: string) => void;
  onSave?: (label: string) => void;
}

export const LabelSelect = (props: Props & Actions) => {
  return (
    <FormGroup>
      {props.title
        ? <ControlLabel>{props.title}</ControlLabel>
        : null
      }
      <InputGroup>
        <InputGroup.Addon
          style={{
            background: props.colors[props.labels.indexOf(props.label)],
            color: 'white'
          }}
        >{Option.of(props.idAddon).get(() => '_')}
        </InputGroup.Addon>
        <ButtonGroup justified={true}>
          <DropdownButton
            id="label-selector"
            componentClass={InputGroup.Button}
            title={props.label}
            style={{width: '100%'}}
          >
            {props.labels.map(label => (
              <MenuItem
                key={label}
                onClick={e => props.onChangeLabel(label)}
              >
                {label}
              </MenuItem>
            ))}
          </DropdownButton>
        </ButtonGroup>
        {Option
          .of(props.onSave)
          .map(onSave => (
            <InputGroup.Button>
              <Button onClick={e => onSave(props.label)}>
                {Option.of(props.saveButton).get(() => '+')}
              </Button>
            </InputGroup.Button>
          ))
          .get(() => <div/>)
        }
      </InputGroup>
    </FormGroup>
  );
};
