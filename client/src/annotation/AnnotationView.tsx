/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {
  Button,
  Col,
  ControlLabel,
  DropdownButton,
  FormControl,
  FormGroup,
  InputGroup,
  Jumbotron,
  MenuItem,
  Row
} from 'react-bootstrap';
import { Actions, HULKS_PINK, ImageColors, ObjectColors, Props } from './AnnotationModel';
import { LabelSelect } from './LabelSelectComponent';
import { KeyBinding } from '../util/KeyBinding';
import { GenericPoint, Point, Points } from '../api/Point';
import './AnnotationView.css';
import { renderIf } from '../util/Reacts';
import { Rect } from './RectComponent';
import { Preview } from './PreviewComponent';
import { Lifecycle } from '../Lifecycle';
import { Option, Range, Stream } from '../util/Types';
import { RectSelector } from './RectSelectorComponent';
import { Line } from './LineComponent';
import { KeyboardLayout } from '../AppModel';

const defaultKeyboard = (props: Props & Actions) => {
  const allLabels = [...props.state.labels.boxes, ...props.state.labels.lines];
  return (
    <div>
      <KeyBinding code="ArrowUp" onKey={e => props.onArrow({e: e, d: {x: 0, y: -.001}})}/>
      <KeyBinding code="ArrowDown" onKey={e => props.onArrow({e: e, d: {x: 0, y: .001}})}/>
      <KeyBinding code="ArrowLeft" onKey={e => props.onArrow({e: e, d: {x: -.001, y: 0}})}/>
      <KeyBinding code="ArrowRight" onKey={e => props.onArrow({e: e, d: {x: .001, y: 0}})}/>
      <KeyBinding code="Escape" onKey={e => props.onResetSelect()}/>
      <KeyBinding code="Enter" shift={true} onKey={e => window.confirm('Are you sure?') ? props.onSave() : null}/>
      <KeyBinding
        code="Enter"
        shift={false}
        onKey={e => {
          props.onCreateLabel();
          props.onResetSelect();
        }}
      />
      {Stream.of(Range(1, Math.min(9, allLabels.length + 1)))
        .map((i: number) => (
          <KeyBinding
            key={i}
            code={`Digit${i}`}
            onKey={e => props.onChangeLabel(
              {
                current: Option
                  .of(allLabels)
                  .map(l => l[i - 1]).get(() => '')
              }
            )}
          />
        )).toArray()}
      {allLabels
        .map(key => (
               <KeyBinding
                 key={key}
                 ctrl={false}
                 code={`Key${key[0].toUpperCase()}`}
                 onKey={e => props.onChangeLabel({current: key})}
               />
             )
        )
      }
      {props.state.labels.images
        .map(key => (
               <KeyBinding
                 key={key}
                 ctrl={true}
                 code={`Key${key[0].toUpperCase()}`}
                 onKey={e => props.onChangeLabel({image: key})}
               />
             )
        )
      }
    </div>
  );
};

const fancyKeyboard = (props: Props & Actions) => {
  const allLabels = [...props.state.labels.boxes, ...props.state.labels.lines];
  const imageLabelKeys = ['KeyR', 'KeyE', 'KeyW', 'KeyQ'];
  const objectLabelKeys = ['KeyF', 'KeyD', 'KeyS', 'KeyA', 'KeyV', 'KeyC', 'KeyX', 'KeyY'];
  return (
    <div>
      <KeyBinding code="KeyJ" onKey={e => props.onArrow({e: e, d: {x: 0, y: -.001}})}/>
      <KeyBinding code="ArrowUp" onKey={e => props.onArrow({e: e, d: {x: 0, y: -.001}})}/>
      <KeyBinding code="KeyK" onKey={e => props.onArrow({e: e, d: {x: 0, y: .001}})}/>
      <KeyBinding code="ArrowDown" onKey={e => props.onArrow({e: e, d: {x: 0, y: .001}})}/>
      <KeyBinding code="KeyH" onKey={e => props.onArrow({e: e, d: {x: -.001, y: 0}})}/>
      <KeyBinding code="ArrowLeft" onKey={e => props.onArrow({e: e, d: {x: -.001, y: 0}})}/>
      <KeyBinding code="KeyL" onKey={e => props.onArrow({e: e, d: {x: .001, y: 0}})}/>
      <KeyBinding code="ArrowRight" onKey={e => props.onArrow({e: e, d: {x: .001, y: 0}})}/>
      <KeyBinding code="Escape" onKey={e => props.onResetSelect()}/>
      <KeyBinding code="Space" shift={true} onKey={e => window.confirm('Are you sure?') ? props.onSave() : null}/>
      <KeyBinding code="Enter" shift={true} onKey={e => window.confirm('Are you sure?') ? props.onSave() : null}/>
      <KeyBinding
        code="Enter"
        shift={false}
        onKey={e => {
          props.onCreateLabel();
          props.onResetSelect();
        }}
      />
      <KeyBinding
        code="Space"
        shift={false}
        onKey={e => {
          props.onCreateLabel();
          props.onResetSelect();
        }}
      />
      {Stream.of(Range(1, Math.min(objectLabelKeys.length + 1, allLabels.length + 1)))
        .map((i: number) => (
          <KeyBinding
            key={i}
            code={objectLabelKeys[i - 1]}
            onKey={e => props.onChangeLabel(
              {
                current: Option
                  .of(allLabels)
                  .map(l => l[i - 1]).get(() => '')
              }
            )}
          />
        )).toArray()}
      {Stream.of(Range(1, Math.min(imageLabelKeys.length + 1, props.state.labels.images.length + 1)))
        .map((i: number) => (
          <KeyBinding
            key={i}
            code={imageLabelKeys[i - 1]}
            onKey={e => props.onChangeLabel(
              {
                image: props.state.labels.images[i - 1]
              }
            )}
          />
        )).toArray()}
    </div>
  );
};

export const Component = (props: Props & Actions) => {
  const stateStart = Points.reduce(props.state.p1, props.state.p2, Math.min);
  const stateEnd = Points.reduce(props.state.p1, props.state.p2, Math.max);
  const stateSize = Points.minus(stateEnd, stateStart);
  const allLabels = [...props.state.labels.boxes, ...props.state.labels.lines];
  return (
    <Row>
      <Lifecycle
        componentDidMount={() => {
          props.onLoadImage({});
          props.onLoadLabels({});
        }}
      />
      {props.keyboard === KeyboardLayout.DEFAULT ? defaultKeyboard(props) : fancyKeyboard(props)}
      <Col xs={12} md={8} lg={6}>
        <Jumbotron
          style={{background: ImageColors[props.state.labels.images.indexOf(props.state.label.image)]}}
          ref={e => preventTouchDefaults(e)}
          onMouseDown={e => props.onMouseDown(e)}
          onMouseMove={e => props.onMouseMove(e)}
          onMouseUp={e => props.onMouseUp(e)}
          onTouchStart={e => props.onTouchStart(e)}
          onTouchMove={e => props.onTouchMove(e)}
          onTouchEnd={e => props.onTouchEnd(e)}
        >
          <div
            id="labelImage"
            className="image draw-panel no-drag"
            draggable={false}
          >
            <img
              src={props.state.image}
              width="100%"
              draggable={false}
              className="no-drag"
            />
            <svg
              style={{
                position: 'absolute',
                top: '0',
                left: '0',
                width: '100%',
                height: '100%'
              }}
            >
              {props.state.box.map((b, i) => (
                <Rect
                  key={i}
                  start={ToPercent(b.start)}
                  size={ToPercent(b.size)}
                  content={`[${i}]`}
                  color={ObjectColors[props.state.labels.boxes.indexOf(b.label)]}
                />
              ))}
              {props.state.lines.map((b, i) => (
                <Line
                  key={i}
                  label={`[${i}]`}
                  p1={b.start}
                  p2={b.end}
                  color={ObjectColors[props.state.labels.lines.indexOf(b.label)]}
                />
              ))}
              {renderIf(!props.state.initial, () =>
                renderIf(
                  props.state.labelType === 'boxes',
                  () => (
                    <RectSelector
                      label={props.state.label.current}
                      p1={props.state.p1}
                      p2={props.state.p2}
                      stateStart={stateStart}
                      stateSize={stateSize}
                    />
                  ),
                  () => (
                    <Line
                      label={props.state.label.current}
                      p1={props.state.p1}
                      p2={props.state.p2}
                      color={HULKS_PINK}
                    />
                  )
                )
              )}
            </svg>
            {renderIf(
              !props.state.initial,
              () => renderIf(
                props.state.labelType === 'boxes',
                () => (
                  <Preview
                    imageUrl={props.state.image}
                    stateStart={stateStart}
                    stateSize={stateSize}
                    left={stateStart.x > (1 - stateEnd.x) ? '0px' : undefined}
                    right={stateStart.x <= (1 - stateEnd.x) ? '0px' : undefined}
                    top={stateStart.y > (1 - stateEnd.y) ? '0px' : undefined}
                    bottom={stateStart.y <= (1 - stateEnd.y) ? '0px' : undefined}
                  />
                ),
                () => (
                  <div>
                    <Preview
                      imageUrl={props.state.image}
                      stateStart={Points.apply(
                        props.state.p1,
                        v => v - 0.02)}
                      stateSize={{x: 0.04, y: 0.04}}
                      width="15%"
                      left={stateStart.x > (1 - stateEnd.x) ? '0px' : undefined}
                      right={stateStart.x <= (1 - stateEnd.x) ? '15%' : undefined}
                      top={stateStart.y > (1 - stateEnd.y) ? '0px' : undefined}
                      bottom={stateStart.y <= (1 - stateEnd.y) ? '0px' : undefined}
                      content={(
                        <div
                          style={{
                            position: 'absolute',
                            top: '48%',
                            left: '48%',
                            width: '4%',
                            height: '4%',
                            background: HULKS_PINK
                          }}
                        />
                      )}
                    />
                    <Preview
                      imageUrl={props.state.image}
                      stateStart={Points.apply(
                        props.state.p2,
                        v => v - 0.02)}
                      stateSize={{x: 0.04, y: 0.04}}
                      width="15%"
                      left={stateStart.x > (1 - stateEnd.x) ? '15%' : undefined}
                      right={stateStart.x <= (1 - stateEnd.x) ? '0px' : undefined}
                      top={stateStart.y > (1 - stateEnd.y) ? '0px' : undefined}
                      bottom={stateStart.y <= (1 - stateEnd.y) ? '0px' : undefined}
                      content={(
                        <div
                          style={{
                            position: 'absolute',
                            top: '48%',
                            left: '48%',
                            width: '4%',
                            height: '4%',
                            background: HULKS_PINK
                          }}
                        />
                      )}
                    />
                  </div>
                )
              ))}
          </div>
        </Jumbotron>
      </Col>
      <Col xs={12} md={4} lg={3}>
        <Jumbotron>
          <LabelSelect
            title="Image Label:"
            colors={ImageColors}
            label={props.state.label.image}
            labels={props.state.labels.images}
            onChangeLabel={l => props.onChangeLabel({image: l})}
          />
          <LabelSelect
            title="Add Label:"
            colors={ObjectColors}
            labels={allLabels}
            label={props.state.label.current}
            onChangeLabel={l => props.onChangeLabel({current: l})}
            onSave={l => {
              props.onCreateLabel();
              props.onResetSelect();
            }}
          />
        </Jumbotron>
      </Col>
      <Col xs={12} md={4} lg={3}>
        <Jumbotron>
          {
            props.state.box.length > 0 ? (
              <form>
                <ControlLabel>
                  Existing Boxes:
                </ControlLabel>
                {props.state.box.map((box, i) => {
                  return (
                    <LabelSelect
                      key={i}
                      idAddon={`${i}`}
                      saveButton={'x'}
                      label={box.label}
                      labels={props.state.labels.boxes}
                      colors={ObjectColors}
                      onChangeLabel={label => props.onChangeExistingLabel({id: i, label: label})}
                      onSave={() => props.onDeleteBox(i)}
                    />
                  );
                })}
              </form>
            ) : null
          }
          {
            props.state.lines.length > 0 ? (
              <form>
                <ControlLabel>
                  Existing Lines:
                </ControlLabel>
                {props.state.lines.map((box, i) => {
                  return (
                    <LabelSelect
                      key={i}
                      idAddon={`${i}`}
                      saveButton={'x'}
                      label={box.label}
                      labels={props.state.labels.lines}
                      colors={ObjectColors}
                      onChangeLabel={label => props.onChangeExistingLabel({id: i, label: label})}
                      onSave={() => props.onDeleteLine(i)}
                    />
                  );
                })}
              </form>
            ) : null

          }
          <Button
            default={false}
            onClick={() => window.confirm('Are you sure?') ? props.onSave() : null}
          >
            Send
          </Button>
        </Jumbotron>
      </Col>
      <Col xs={12} md={8} lg={6}>
        <Jumbotron>
          <form>
            <FormGroup>
              <InputGroup>
                <DropdownButton
                  componentClass={InputGroup.Button}
                  id="input-dropdown-addon"
                  title="Previous"
                >
                  {props.state.previousImages
                    .map((image, i) =>
                           <MenuItem
                             key={i}
                             onClick={e => props.onChangeLoadImage(image)}
                           >
                             {image}
                           </MenuItem>
                    )}
                </DropdownButton>
                <FormControl
                  type="text"
                  value={props.state.loadImage}
                  onChange={e => props.onChangeLoadImage((e.target as HTMLInputElement).value)}
                />
                <InputGroup.Button>
                  <Button
                    onClick={e => props.onLoadSpecificImage(props.state.loadImage)}
                  >
                    Load
                  </Button>
                </InputGroup.Button>
              </InputGroup>
            </FormGroup>
          </form>
        </Jumbotron>
      </Col>
    </Row>
  );
};

export const ToPercent = (p: Point): GenericPoint<string> => Points.apply(p, v => `${v * 100}%`);

const preventTouchDefaults = (el: Jumbotron | null) => {
  if (el) {
    ReactDOM.findDOMNode(el).addEventListener('touchstart', e => e.preventDefault());
    ReactDOM.findDOMNode(el).addEventListener('touchmove', e => e.preventDefault());
    ReactDOM.findDOMNode(el).addEventListener('touchend', e => e.preventDefault());
  }
};
