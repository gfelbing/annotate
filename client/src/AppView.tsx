/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import { AnnotateImage } from './annotation/AnnotationController';
import { Button, ButtonGroup, Col, Grid, Navbar, Row } from 'react-bootstrap';
import { Actions, KeyboardLayout, Props } from './AppModel';
import { Score } from './scores/ScoreController';
import { Readme } from './readme/ReadmeView';
import { LoginButton } from './login/LoginController';

export const Component = (props: Props & Actions) => {
  return (
    <div>
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            annotate!
          </Navbar.Brand>
          <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
          <Navbar.Form pullRight={true}>
            <ButtonGroup>
              <Button disabled={true}>
                Keyboard:
              </Button>
              {Object.keys(KeyboardLayout)
                .filter(k => isNaN(Number(k)))
                .map((key, index) => (
                  <Button
                    key={index}
                    active={index.toString() === props.state.keyboard.toString()}
                    onClick={() => props.onUpdateKeyboard(key)}
                  >
                    {key}
                  </Button>
                ))}
            </ButtonGroup>
          </Navbar.Form>
          <Navbar.Form pullRight={true}>
            <LoginButton/>
          </Navbar.Form>
        </Navbar.Collapse>
      </Navbar>
      <Grid>
        <AnnotateImage
          authToken={props.authToken}
          keyboard={props.state.keyboard}
        />
        <Readme keyboard={props.state.keyboard}/>
        <Row>
          <Col xs={12} md={12}>
            <Score/>
          </Col>
        </Row>
      </Grid>
    </div>
  );
};
