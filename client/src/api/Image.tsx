/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Point } from "./Point";

export interface PostImage {
  label: Label
  authToken?: string
}

export interface GetAuth {
  code: string
}

export interface Label {
  url: string,
  label?: string,
  boxes: Box[],
  lines?: Line[],
  labelCount?: number
}

export interface Box {
  label: string,
  start: Point,
  size: Point
}

export interface Line {
  label: string,
  start: Point,
  end: Point
}
