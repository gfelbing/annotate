/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Point, Points } from '../api/Point';

export function renderIf<T>(predicate: boolean, then: () => T, fallback: () => T | null = () => null): T | null {
  if (predicate) {
    return then();
  }
  return fallback();
}

export const fromMouseClick = (mouseEvent: React.MouseEvent<{}>): Point => {
  return {
    x: mouseEvent.clientX,
    y: mouseEvent.clientY
  };
};

export const fromTouchClick = (touchEvent: React.TouchEvent<{}>): Point => {
  return {
    x: touchEvent.targetTouches[0].clientX,
    y: touchEvent.targetTouches[0].clientY
  };
};

export const fromMouseEvent = (clickPoint: Point, mouseEvent: React.MouseEvent<{}> | React.TouchEvent<{}>): Point => {
  const element = (mouseEvent.currentTarget as Element);
  const image = element.getElementsByClassName('image')[0].getBoundingClientRect();
  const imageOrigin: Point = {
    x: image.left,
    y: image.top
  };
  const imageSize: Point = {
    x: image.width,
    y: image.height
  };
  const capped: Point = Points.cap(
    Points.minus(clickPoint, imageOrigin),
    {x: 0, y: 0},
    imageSize
  );
  const relative: Point = Points.reduce(
    capped,
    imageSize,
    (c, i) => c / i
  );
  return relative;
};
