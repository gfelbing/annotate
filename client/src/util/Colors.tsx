/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

export function ColorCount(no: number): string {
  const first = no % 2;
  const second = Math.round(no / 2) % 2;
  const third = Math.round(no / 4) % 2;
  return `#${[third, second, first].map(noToHex).join('')}`;
}

function noToHex(no: number): string {
  switch (no) {
    case 0:
      return '00';
    case 1:
      return 'AA';
    default:
  }
  return '';
}
