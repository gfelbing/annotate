/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Button, Modal } from 'react-bootstrap';
import * as React from 'react';

export const ConfirmDialog = (props: {
  title?: string,
  body: string,
  yes?: string,
  no?: string,
  onYes?: () => void,
  onNo?: () => void
}) => (
  <div className="static-modal">
    <Modal.Dialog>
      <Modal.Header>
        <Modal.Title>
          {props.title ? props.title : ""}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {props.body}
      </Modal.Body>
      <Modal.Footer>
        <Button
          onClick={e => {
            if (props.onNo) {
              props.onNo();
            }
          }}
        >
          {props.no ? props.no : "Cancel"}
        </Button>
        <Button
          bsStyle="primary"
          onClick={e => {
            if (props.onYes) {
              props.onYes();
            }
          }}
        >
          {props.yes ? props.yes : "Ok"}
        </Button>
      </Modal.Footer>

    </Modal.Dialog>
  </div>
);
