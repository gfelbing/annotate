/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-theme.min.css';
import { combineReducers, createStore } from 'redux';
import { Reducer as AnnotationReducer } from './annotation/AnnotationController';
import { State as AnnotationState } from './annotation/AnnotationModel';
import { Provider } from 'react-redux';
import { State as AppState } from './AppModel';
import { State as LoginState } from './login/LoginModel';
import { Reducer as LoginReducer } from './login/LoginController';
import { App, AppReducer } from './AppController';
import { State as ScoreState } from './scores/ScoreModel';
import { Reducer as ScoreReducer } from './scores/ScoreController';

export interface RootState {
  annotation: AnnotationState;
  app: AppState;
  login: LoginState;
  scores: ScoreState;
}

const store = createStore(
  combineReducers(
    {
      app: AppReducer,
      annotation: AnnotationReducer,
      login: LoginReducer,
      scores: ScoreReducer
    })
);

ReactDOM.render(
  <Provider store={store}>
    <App
      authToken={(store.getState() as RootState).login.login.map(l => l.accessToken)}
    />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
