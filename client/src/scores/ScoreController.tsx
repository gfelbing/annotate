/*
 * This file is part of annotate.
 *
 * annotate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ReducerFactory } from '../util/Reducers';
import { Actions, OwnProps, Props, State } from './ScoreModel';
import { connect, Dispatch } from 'react-redux';
import { RootState } from '../index';
import { Scores } from '../api/Scores';
import axios from 'axios';
import { Component } from './ScoreView';
import { Stats } from '../api/Stats';

const actions = new ReducerFactory<State, OwnProps>();

const RequestScores = actions.createVoidAction((state, action) => {
  axios.get('/v1/scores')
    .then(r => r.data as Scores)
    .then(s => HandleScores.create(action.dispatch, action.props)(s));
  return state;
});

const HandleScores = actions.createAction<Scores>((s, a) => {
  return {
    ...s,
    scores: a.value
  };
});

const RequestStats = actions.createVoidAction((state, a) => {
  axios.get('/v1/stats')
    .then(r => r.data as Stats)
    .then(s => HandleStats.create(a.dispatch, a.props)(s));
  return state;
});

const HandleStats = actions.createAction<Stats>((s, a) => ({
  ...s,
  stats: a.value
}));

const initialState: State = {
  scores: {},
  stats: {
    labeled: -1,
    remaining: -1
  }
};
export const Reducer = actions.build(initialState);

export const Score = connect(
  (rootState: RootState, ownProps: OwnProps): Props => ({
    state: rootState.scores
  }),
  (dispatch: Dispatch<{}>, ownProps: OwnProps): Actions => ({
    onLoad: () => {
      RequestScores.create(dispatch, ownProps)();
      RequestStats.create(dispatch, ownProps)();
    }
  })
)(Component);
